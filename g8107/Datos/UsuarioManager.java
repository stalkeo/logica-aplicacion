package g8107.Datos;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class UsuarioManager {

	private EntityManagerFactory emf;

	public UsuarioManager(){
	}

	/*Se deve pasar por parametro el ENtityManagerFactory correspondiente*/
	public UsuarioManager(EntityManagerFactory emf) {
		this.emf = emf;
	}

	/*Se permite el cambio de EntityManagerFactory*/
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf = emf;
	}

	/*Devuelve un EntityManager, si el EntityManagerFactory no esta creado devolverá null*/
	private EntityManager getEntityManager() {
		if (emf == null) {
			throw new RuntimeException(
					"The EntityManagerFactory is null.  This must be passed in to the constructor or set using the setEntityManagerFactory() method.");
		}
		return emf.createEntityManager();
	}

	/*Inserta un usuario en la base de datos*/
	private boolean createUsuario(Usuario Usuario) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(Usuario);
			em.getTransaction().commit();
		} 
		catch (Exception ex) {
			try {
				if (em.getTransaction().isActive())
					em.getTransaction().rollback();
			} 
			catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} 
		finally {
			em.close();
		}
		return true;
	}

	private boolean deleteUsuario(Usuario Usuario) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			Usuario = em.merge(Usuario);
			em.remove(Usuario);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return true;
	}

	private boolean updateUsuario(Usuario Usuario) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			Usuario = em.merge(Usuario);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return true;
	}

	public Usuario findUsuarioByEmail(String eMail) {
		Usuario Usuario = null;
		EntityManager em = getEntityManager();
		
		try {
			Usuario = (Usuario) em.find(Usuario.class, eMail);
		} 
		finally {
			em.close();
		}
		return Usuario;
	}

	/*Función de validación de los usuarios*/
	public boolean validateUser(String eMail, String pass) {
		Usuario Usuario = null;
		Usuario = findUsuarioByEmail(eMail);
		if(Usuario !=null){
			if(Usuario.getEMail().equals(eMail) && Usuario.getPassword().equals(pass))
				return true;
		}
		return false;
	}
	
	/*Función de creación de nuevos usuarios*/
	public boolean insertarUser(String e_mail, String pass, String nombre, String ap1, String ap2, String cod_postal, String provincia)  {
		Usuario Usuario = new Usuario(e_mail, pass, nombre, ap1,  ap2,  Integer.parseInt(cod_postal), provincia,false);
		try {
			if(createUsuario(Usuario)) return true;
		} 
		catch (Exception e) {
			/*La excepción no pasa al servlet*/
			e.printStackTrace();
		}
		return false;
	}
	
	/*Función que modifica los datos de los usuarios*/
	public boolean ModificarUser(String e_mail, String pass, String nombre, String ap1, String ap2, String cod_postal, String provincia, String pass_old)  {
		Usuario Usuario = findUsuarioByEmail(e_mail);
		if(!pass_old.isEmpty()){
			if(!Usuario.getPassword().equals(pass_old))
				return false;
			Usuario.setPassword(pass);
		}
		else{
			if(!nombre.isEmpty())
				Usuario.setNombre(nombre);
			if(!ap1.isEmpty())
				Usuario.setApellido1(ap1);
			if(!ap2.isEmpty())
				Usuario.setApellido2(ap2);
			if(!cod_postal.isEmpty())
				Usuario.setCodPostal(Integer.parseInt(cod_postal));
			if(!provincia.isEmpty())
				Usuario.setCiudad(provincia);
		}
		
		try {
			if(updateUsuario(Usuario)) return true;
		} 
		catch (Exception e) {
			/*La excepción no pasa al servlet*/
			e.printStackTrace();
		}
		System.out.println("hola7");
		return false;
	}
	
	
	/*Función de dar de baja usuarios*/
	public boolean BajaUser(String e_mail, String contrasena)  {
		Usuario Usuario = findUsuarioByEmail(e_mail);
		if(!contrasena.isEmpty()){
			if(Usuario.getPassword().equals(contrasena)){
				try {
					if(deleteUsuario(Usuario)) return true;
				} 
				catch (Exception e) {
					/*La excepción no pasa al servlet*/
					e.printStackTrace();
				}
			}
		}
		return false;
	}

}

