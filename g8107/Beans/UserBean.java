package g8107.Beans;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import g8107.Datos.Usuario;
import g8107.Datos.UsuarioManager;

public class UserBean implements java.io.Serializable{
	
	private static final long serialVersionUID = 1L;

	private String e_mail;

	private String apellido1;

	private String apellido2;

	private String ciudad;

	private String codPostal;

	private String nombre;
	
	
	public UserBean(){
		
	}

	public String getE_mail() {
		return e_mail;
	}

	/*El email que se debe pasar es el de la sesion del usuario para obtener toda la infromacion referente a este*/
	public void setE_mail(String e_mail) {
		
		EntityManagerFactory emf= Persistence.createEntityManagerFactory("g8107-Wallapop");
		UsuarioManager userManager = new UsuarioManager(emf);
		Usuario usuario = userManager.findUsuarioByEmail(e_mail);
		
		this.e_mail = usuario.getEMail();
		this.apellido1 = usuario.getApellido1(); 
		this.apellido2 = usuario.getApellido2();
		this.ciudad = usuario.getCiudad();
		this.codPostal = Integer.toString(usuario.getCodPostal());
		this.nombre = usuario.getNombre();
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCodPostal() {
		return codPostal;
	}

	public void setCodPostal(String codPostal) {
		this.codPostal = codPostal;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
