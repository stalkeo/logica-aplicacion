package g8107.Beans;

import javax.imageio.ImageIO;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import g8107.Datos.ImagenManager;
import g8107.Datos.Imagene;
import g8107.Datos.Producto;
import g8107.Datos.ProductoManager;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class IndexBean implements java.io.Serializable{
	
	private static final long serialVersionUID = 1L;

	private List<Producto> list;
	private List <String> img_path = new ArrayList<String> ();
	private String path = "F:/Workspace/Servidores/GlassFish 4.0/glassfish4/glassfish/domains/domain1/eclipseApps/g8107-EAR/g8107-Wallapop_war/temp/";
	
	public IndexBean (){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("g8107-Wallapop");
		ProductoManager productManager = new ProductoManager(emf);
		ImagenManager imagenmanager = new ImagenManager(emf);
		
		// Buscamos los productos de la BBDD
		list = productManager.advSearch("Todo", "", "", "", "");	
		
		// Solo queremos mostrar un numero limitado, por lo que eliminamos los productos que no necesitamos.
		while (list.size() > 12){
			list.remove(list.size() - 1);			
		}
		
		if(list !=null && !list.isEmpty()){
			Imagene img10 = null;
			String img_name10 = "";
			BufferedImage img_buff10 = null;
			
			
			for (int i = 0; i < list.size(); i++){				
				/*Obtenemos las imagenes*/
				img10 = imagenmanager.obtenerImagenesByProducto(list.get(i).getId()).get(0);				
				
				img_name10 = new Integer(list.get(i).getId()).toString() + new Integer(img10.getId().getNumImg()).toString();
				
				try {
					img_buff10 = ImageIO.read(new ByteArrayInputStream(img10.getImagen()));
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				
				File img_file = new File (path + img_name10 + ".jpg");
				
				try {
					ImageIO.write(img_buff10, "jpg", img_file);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				img_path.add("temp/" + img_name10 + ".jpg");
			}
			
		
		}
		
		
	}
	
	public List<Producto> getList() {
		return list;
	}

	public void setList(List<Producto> list) {
		this.list = list;
	}

	public List<String> getImg_path() {
		return img_path;
	}

	public void setImg_path(List<String> img_path) {
		this.img_path = img_path;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
}
	