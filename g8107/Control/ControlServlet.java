package g8107.Control;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import g8107.Chat.JMSManager;
import g8107.Datos.ImagenManager;
import g8107.Datos.Imagene;
import g8107.Datos.ImagenePK;
import g8107.Datos.Producto;
import g8107.Datos.ProductoManager;
import g8107.Datos.Usuario;
import g8107.Datos.UsuarioManager;
import sun.misc.IOUtils;

import javax.imageio.ImageIO;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.RequestDispatcher;

import javax.servlet.http.Part;
/**
 * Servlet implementation class ControlServlet
 */

@WebServlet(name="ControlServlet",urlPatterns={"/ControlServlet"})
@MultipartConfig(maxFileSize = 16177215)
public class ControlServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	private String path = "C:/Users/juan0/Documents/Servidores/glassfish-4.0-ml/glassfish4/glassfish/domains/domain1/eclipseApps/g8107-EAR/g8107-Wallapop_war/temp/";
	//private String path = "F:/Workspace/Servidores/GlassFish 4.0/glassfish4/glassfish/domains/domain1/eclipseApps/g8107-EAR/g8107-Wallapop_war/temp/";

	//private String path = "http://localhost:8080/g8107-Wallapop/img";	
	

	ServletContext miServletContex = null;
	
    public ControlServlet() {
        super();
    }
    
    public void init() {
    	//Se coge el contexto de servlet
		miServletContex = getServletContext();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Se declara el requestdispatcher.
		RequestDispatcher reqDis;
		
		/*Se declara el ENtityManagerFactory que se va utilizar para todas las peticiones a la base de datos*/
		EntityManagerFactory emf= Persistence.createEntityManagerFactory("g8107-Wallapop");
		
		/*Declaración de el objeto userManager que realizará los cambios en la tabla de usuarios*/
		UsuarioManager userManager = new UsuarioManager(emf);
		
		//Variable auxiliar para obtener el aprametro que indica que tipo de peticion se realiza.
		int aux = Integer.parseInt(request.getParameter("n_peticion"));
				
		/*Declaracion del objeto productManager que realizara los cambios en la tabla productos*/
		ProductoManager productManager = new ProductoManager(emf);
		
		ImagenManager imagenmanager = new ImagenManager(emf);
		
		switch(aux){
		/*Inicio de sesion*/
		case 1:	
			
			//Variables necesarias
			boolean validacion;
			
			//Establecemos la conexion a la base de datos y se realiza la consulta.
			validacion = userManager.validateUser(request.getParameter("e_mailL"),request.getParameter("contrasenaL"));
			
			//Si al validacion es correcta
			if(validacion){
				System.out.println("Nuevo inicio de sesion");
				HttpSession session = request.getSession(true);
				session.setAttribute("e_mail",request.getParameter("e_mailL"));
				reqDis = request.getRequestDispatcher("index.jsp");
				reqDis.forward(request, response);
			}
			
			else{
				System.out.println("Fallo en el inicio de sesion");
				HttpSession session = request.getSession(true);
				session.setAttribute("err", "Parece que ha habido un fallo en el inicio de sesi�n.");
				reqDis = request.getRequestDispatcher("errorHasOccurred.jsp");
				reqDis.forward(request, response);
			}
			
			break;
			
		/*Nuevo registro */	
		case 2:
			
			//Booleano que indica si se ha realizado correctamente una insercción
			boolean insercion;
			
			//Se establece la conexión con la base de datos y se intenta insertar el usuario
			insercion = userManager.insertarUser(request.getParameter("e_mailR"), request.getParameter("contrasenaR"), request.getParameter("nombre"), request.getParameter("apellido1"), request.getParameter("apellido2"), request.getParameter("cod_postal"), request.getParameter("provincia"));
			
			//Si se inserta correctamente se vuelve al inicio con la nueva cuenta creada
			if(insercion){
				System.out.println("Nuevo registro de usuario realizado con e_mail: " +request.getParameter("e_mailR"));
				HttpSession session = request.getSession(true);
				session.setAttribute("e_mail",request.getParameter("e_mail"));
				reqDis = request.getRequestDispatcher("index.jsp");
				reqDis.forward(request, response);
			}
			
			//Si sucede un error se lleva a la pagina de error
			else{
				System.out.println("No se ha realizado el nuevo registro");
				HttpSession session = request.getSession(true);
				session.setAttribute("err", "Parece que ha habido un fallo en el registro.");
				reqDis = request.getRequestDispatcher("errorHasOccurred.jsp");
				reqDis.forward(request, response);
			}
			break;
			
		/*Ir al perfil del usuario */	
		case 3 :
			/*Se comprueba que hay session*/
			if(request.getSession(false) == null){
				System.out.println("Se ha intentado ir al perfil sin sesion o sesion caducada");
				HttpSession session = request.getSession(true);
				session.setAttribute("err", "Parece que no ha iniciado sesi�n, o que esta ha caducado.");
				reqDis = request.getRequestDispatcher("errorHasOccurred.jsp");
				reqDis.forward(request, response);
			}
			
			
			/*Obtenemos los productos del usuario que va a acceder a su perfil*/			
			List <Producto> productosUsuario = productManager.findProductoByPropietario(request.getSession().getAttribute("e_mail").toString());
			System.out.println("productosUsuario tiene estas iteraciones: " + productosUsuario.size());
			/*Obtenemos las imagenes de los productos y los insertamos en la lista de imagenes de esos productos*/
					
			
			InputStream in;
			BufferedImage fichero_imagen;
			String img_name = "";
			Imagene img;
			List <String> rutas = new ArrayList <String> ();		
			
			
			for (int i = 0; i < productosUsuario.size(); i++){
				
				/*Para cada producto, cogemos su imagen principal*/	
				img = imagenmanager.obtenerImagenesByProducto(productosUsuario.get(i).getId()).get(0);
				
				/*Establecemos el nombre del archivo en funcion de el ID del rpdocuto y el numero de imagen*/
				img_name = Integer.toString(img.getId().getIdProd()) + img.getId().getNumImg();
	
				
				in = new ByteArrayInputStream(img.getImagen());
				
				fichero_imagen = ImageIO.read(in);
				
				File img_save = new File (path + img_name + ".jpg");
				
			//	System.out.println(path + "//" + img_name + ".jpg");
				ImageIO.write(fichero_imagen, "jpg", img_save);
				
				//path = img_save.getAbsolutePath();
				
				/*A�adimos las rutas que m�s tarde el JSP leer�*/
				rutas.add("temp/" + img_name + ".jpg");
				//rutas.add(path + '/' + img_name + ".jpg");
			}
			
			
					/*file://C:/Users/juan0/Desktop/camara.jpg*/
			
			/*Impresion para debugear*/
			//System.out.println("Titulo del productoUsuario[0] " +productosUsuario.get(0).getTitulo());
			
			
			/*Si el usuario tiene algun producto, los introducimos en la request para obtenerlos desde el JSP*/
			//System.out.println("El tama�o de productosUsuario es " + productosUsuario.size());
				
				/*Introducimos esos productos (List <Producto>) en la request para obtenerlos desde el JSP*/
				request.setAttribute("productosUsuario", productosUsuario);
				
				/*Introducimos las rutas (List <String>) en la request para obtenerlos desde el JSP*/
				request.setAttribute("rutas", rutas);
			
			
			/*Si hay sesion se conduce al usuario a su perfil*/
			reqDis = request.getRequestDispatcher("perfil.jsp");
			reqDis.forward(request, response);
			break;
			
			
		/*Ir a los productos del usuario */	
		case 4:
			/*Se comprueba que hay session*/
			if(request.getSession(false) == null){
				System.out.println("Se ha intentado ir a los productos del usuario sin sesion o sesion caducada");
				HttpSession session = request.getSession(true);
				session.setAttribute("err", "Parece que no ha iniciado sesi�n, o que esta ha caducado.");
				reqDis = request.getRequestDispatcher("errorHasOccurred.jsp");
				reqDis.forward(request, response);
			}
			
			/*Si hay sesion se conduce al usuario a su perfil*/
			reqDis = request.getRequestDispatcher("perfil.jsp");
			reqDis.forward(request, response);
			break;
			
		/*Dar de alta un nuevo producto */	
		case 5:
			/*Se comprueba que hay session*/
			if(request.getSession(false) == null){
				System.out.println("Se ha intentado dar de alta un producto sin sesion o sesion caducada");
				HttpSession session = request.getSession(true);
				session.setAttribute("err", "Parece que no ha iniciado sesi�n, o que esta ha caducado.");
				reqDis = request.getRequestDispatcher("errorHasOccurred.jsp");
				reqDis.forward(request, response);
			}
			
			/*Insertamos un nuevo producto haciendo uso de la clase ProductoManager*/
			/*Por hacer: generar un id del producto, disparador que aumente / disminuya num_img, sacar el Usuario que esta metiendo el producto request.getSession().getAttribute("e_mail"), cambiar script DB Double*/
			/*public boolean insertarProducto(int id, String categoria, int estado, int numImg, BigDecimal precio, String titulo, List<Imagenes> imagenes, Usuario propietario){*/
			
			/*Obtenemos el propietario del usario*/
			//System.out.println("HoliJiji");
			Usuario usuarioPropietario = userManager.findUsuarioByEmail(request.getSession().getAttribute("e_mail").toString());
			
			/* Si no tiene imagenes, el arrayList de imagenes que le meto == null?*/
			List<Imagene> imagenes = null; 
			
			
			/*Insertamos el producto*/
			
			/*El disparador aumentara el id del producto automaticamente*/					
			productManager.insertarProducto(1, request.getParameter("category"), 0, 0, Float.parseFloat(request.getParameter("precioP")), request.getParameter("nombreP"), request.getParameter("Comentarios"), imagenes, usuarioPropietario );
			
			
			Producto productoInsertado = productManager.findProductoByID(productManager.findProductoByIdUltimo().get(0).getId());
			
			InputStream inputStream;
			Part filePart;
			ImagenePK idPK = new ImagenePK ();
			
			
			
			String imagenRequest = "";
			/*Obtiene la imagen de la request*/
			
			for (int i = 0; i < 4; i++){
				
				inputStream = null;
				filePart = null;
				
				
				
				switch (i){
					
					case (0):
						
						imagenRequest = "imagenP1";
						break;
						
					case (1):
						
						imagenRequest = "imagenP2";
						break;
						
					case (2):
						
						imagenRequest = "imagenP3";
						break;
						
					case (3):
					
						imagenRequest = "imagenP4";
						break;
					
					default:			
				}
				
				
				filePart = request.getPart(imagenRequest);
				
				if (filePart != null && filePart.getSize()> 0 && !filePart.getContentType().equals("application/octet-stream")) {
		            
		            System.out.println(filePart.getName());
		            System.out.println(filePart.getSize());
		            System.out.println(filePart.getContentType());
		             
		            /*Obtenemos el inputStream de la imagen*/
		            inputStream = filePart.getInputStream();
		            
				
					//ImagenManager imagenmanager = new ImagenManager (emf);
					
					/*El id del de la imagen (referencia al producto) debe ser el id del ultimo producto que hemos insertado*/
					/*El valor de num_img se actualiza solo por disparador*/
					
					idPK.setIdProd(productManager.findProductoByIdUltimo().get(0).getId());
					idPK.setNumImg(1);
					
					System.out.println("Los valores de idPK son " + idPK.getIdProd() +", " + idPK.getNumImg());
					
					
					/*Pasamos el inputStream a un array de bytes[]*/	
					
					byte[] data = new byte[(int) filePart.getSize()];
					int nRead;
					ByteArrayOutputStream buffer = new ByteArrayOutputStream();
					while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
					  buffer.write(data, 0, nRead);
					}
		
					buffer.flush();
					
					imagenmanager.insertarImagen(idPK, buffer.toByteArray(), productoInsertado);
				}
			}
			
			
			reqDis = request.getRequestDispatcher("index.jsp");
			reqDis.forward(request, response);
			
			/*Insertamos una nueva imagen */
						
			///RecogerImagen rI = new RecogerImagen(request);
			///FileInputStream fis = rI.prepararImagenInserccion();
			
			/*Creamos un array de bytes de tama�o igual al archivo*/
			///byte [] byteArrayImagen = new byte [fis.available()];
			
			/*Pasamos el FileINputStream a un array de bytes*/
			///fis.read(byteArrayImagen);
			
			///ImagenManager iM = new ImagenManager (emf);
			
			/*Por hacer: sacar el id del producto del cual estamos metiendo la imagen*/
			///ImagenePK idPK = new ImagenePK(0, 0);
			
			
			/*Insetamos la imagen en la DB*/
			///iM.insertarImagen(idPK, byteArrayImagen, productoInsertado);
			
			
			
			/*Insertamos una nueva imagen */
			
			
			
			break;
			
		/*Cierre de sesión */	
		case 6:
			/*Se comprueba que hay session*/
			if(request.getSession(false) == null){
				System.out.println("Se ha intentado cerrar una sesión sin sesion o sesion caducada");
				HttpSession session = request.getSession(true);
				session.setAttribute("err", "Parece que no ha iniciado sesi�n, o que esta ha caducado.");
				reqDis = request.getRequestDispatcher("errorHasOccurred.jsp");
				reqDis.forward(request, response);
			}
			
			System.out.println("Cierre de session del usuario con e_mail: " + request.getSession(false).getAttribute("e_mail"));
			request.getSession(false).invalidate();
			reqDis = request.getRequestDispatcher("index.jsp");
			reqDis.forward(request, response);
			break;
			
		/*Modificar datos user */	
		case 7:
			/*Se comprueba que hay session*/
			if(request.getSession(false) == null){
				System.out.println("Se ha intentado cerrar una sesión sin sesion o sesion caducada");
				HttpSession session = request.getSession(true);
				session.setAttribute("err", "Parece que no ha iniciado sesi�n, o que esta ha caducado.");
				reqDis = request.getRequestDispatcher("errorHasOccurred.jsp");
				reqDis.forward(request, response);
			}
			
			//Booleano que indica si se ha realizado correctamente una actualizacion
			boolean actualizacion;
			
			//Se establece la conexión con la base de datos y se intenta actualizar el usuario
			actualizacion = userManager.ModificarUser((String) request.getSession().getAttribute("e_mail"), request.getParameter("pass1"), request.getParameter("nombre"), request.getParameter("apellido1"), request.getParameter("apellido2"), request.getParameter("cod_postal"), request.getParameter("provincia"),request.getParameter("pass_old"));
			
			//Si se actualiza correctamente se vuelve a la pagina de perfil del usuario
			if(actualizacion){
				System.out.println("Se ha modificado el usuario con e_mail: " +(String) request.getSession().getAttribute("e_mail"));
				reqDis = request.getRequestDispatcher("perfil.jsp");
				reqDis.forward(request, response);
			}
			
			//Si sucede un error se lleva a la pagina de error
			else{
				System.out.println("Ha sucedido un error en la actualizacion del usuario con e_mail:" +(String) request.getSession().getAttribute("e_mail"));
				HttpSession session = request.getSession(true);
				session.setAttribute("err", "Parece que ha habido un error en el servidor.");
				reqDis = request.getRequestDispatcher("errorHasOccurred.jsp");
				reqDis.forward(request, response);
			}
			break;
			
		/*Dar de baja user */	
		case 8:
			/*Se comprueba que hay session*/
			if(request.getSession(false) == null){
				System.out.println("Se ha intentado cerrar una sesión sin sesion o sesion caducada");
				HttpSession session = request.getSession(true);
				session.setAttribute("err", "Parece que no ha iniciado sesi�n, o que esta ha caducado.");
				reqDis = request.getRequestDispatcher("errorHasOccurred.jsp");
				reqDis.forward(request, response);
			}
			
			//Booleano que indica si se ha realizado correctamente una baja
			boolean baja;
			
			//Se establece la conexión con la base de datos y se intenta borrar el usuario
			baja = userManager.BajaUser((String) request.getSession().getAttribute("e_mail"),request.getParameter("contrasena"));
			
			//Si se inserta correctamente se vuelve a la pagina de inicio
			if(baja){
				System.out.println("Se ha borrado el usuario con e_mail: " +(String) request.getSession().getAttribute("e_mail"));
				//Se borra la sesion actual
				request.getSession(false).invalidate();
				reqDis = request.getRequestDispatcher("index.jsp");
				reqDis.forward(request, response);
			}
			
			//Si sucede un error se lleva a la pagina de error
			else{
				System.out.println("Ha sucedido un error en borrado del usuario con e_mail:" +(String) request.getSession().getAttribute("e_mail"));
				HttpSession session = request.getSession(true);
				session.setAttribute("err", "Parece que ha habido un error en el servidor.");
				reqDis = request.getRequestDispatcher("errorHasOccurred.jsp");
				reqDis.forward(request, response);
			}
			break;
			
		/*Ir a un producto*/
		case 9:
			

			/*System.out.println("El precioPerfil recibido es: " + request.getParameter("precioPerfil"));
			System.out.println("El nombrePerfil recibido es: " + request.getParameter("nombrePerfil"));
			System.out.println("El categoriaPerfil recibido es: " + request.getParameter("categoriaPerfil"));
			System.out.println("El usuarioPerfil recibido es: " + request.getParameter("usuarioPerfil"));
			System.out.println("El id oculto recibido es: " + request.getParameter("id_oculto"));*/
			
			/*Introducimos los valores recibidos para llevarlos a product.jsp*/
			
			request.setAttribute("precioPerfil", request.getParameter("precioPerfil"));
			request.setAttribute("nombrePerfil", request.getParameter("nombrePerfil"));
			request.setAttribute("categoriaPerfil", request.getParameter("categoriaPerfil"));
			request.setAttribute("usuarioPerfil", request.getParameter("usuarioPerfil"));
			
			
			
			/*Ahora debemos introducir las imagenes (path) del producto para mostrarlas en product.jsp*/
			Producto recibido = productManager.findProductoByID(Integer.parseInt(request.getParameter("id_oculto")));
			
			System.out.println("El id de producto con el que se ha buscado al producto recibido es: " + request.getParameter("id_oculto"));
			System.out.println("El nombre del producto recibido es: " + recibido.getTitulo());
			System.out.println("El numero de imagenes de recibido es: " + recibido.getNumImg());
			
			request.setAttribute("descripcionPerfil", recibido.getDescripcion());
			request.setAttribute("estadoPerfil", recibido.getEstado());
			request.setAttribute("id", recibido.getId());
			request.setAttribute("emailPerfil", recibido.getUsuario().getEMail());
			
		//	Usuario usuarioPropietarioR = userManager.findUsuarioByEmail(request.getSession().getAttribute("e_mail").toString());
			
			request.setAttribute("ciudad", recibido.getUsuario().getCiudad());
			request.setAttribute("codPostal", recibido.getUsuario().getCodPostal());
			
			request.setAttribute("user_visitor", request.getSession().getAttribute("e_mail").toString());
			request.setAttribute("user_owner", recibido.getUsuario().getEMail());

			
			Imagene imgPR;
			String img_nameR = "";
			InputStream inR;
			BufferedImage fichero_imagenR;
			List <String> rutasR = new ArrayList <String> ();	
			
			
			/*Nos recorremos las imagenes del producto del que venimos y las escribimos*/
			for (int i = 0; i < recibido.getNumImg(); i++){
				
				/*Obtenemos las imagenes*/
				imgPR = imagenmanager.obtenerImagenesByProducto(Integer.parseInt(request.getParameter("id_oculto"))).get(i);
				
				//int hola = imgPR.getId().getNumImg();
				//System.out.println("El numImg obtenido es: " + hola);
				
				/*Establecemos el nombre del archivo en funcion de el ID del rpdocuto y el numero de imagen*/
				img_nameR = Integer.toString(imgPR.getId().getIdProd()) + imgPR.getId().getNumImg();
				
				inR= new ByteArrayInputStream(imgPR.getImagen());
				
				fichero_imagenR = ImageIO.read(inR);
				
				File img_saveR = new File (path + img_nameR + ".jpg");
				
				ImageIO.write(fichero_imagenR, "jpg", img_saveR);
				
				rutasR.add("temp/" + img_nameR + ".jpg");
				System.out.println("La ruta introducida es: " + rutasR.get(i));
			}
			
			request.setAttribute("rutasR", rutasR);
			request.setAttribute("numImgR", recibido.getNumImg());
			System.out.println("El valor de numImgR metido es: " + recibido.getNumImg());
			
			/*request.setAttribute("productosUsuario", productosUsuario);*/
			
			reqDis = request.getRequestDispatcher("product.jsp");
			reqDis.forward(request, response);
			break;
			
	
		case 10:		//Busqueda avanzada
			// Se comprueba si hayu una sesion activa
			if(request.getSession(false) == null){
				System.out.println("Se ha intentado cerrar una sesion sin sesion o sesion caducada");
				HttpSession session = request.getSession(true);
				session.setAttribute("err", "Parece que no ha iniciado sesi�n, o que esta ha caducado.");
				reqDis = request.getRequestDispatcher("errorHasOccurred.jsp");
				reqDis.forward(request, response);
			}
			
			System.out.println("====== Busqueda avanzada ======");
			System.out.println("Categoria: " + request.getParameter("categoria"));
			System.out.println("Provincia: " + request.getParameter("provincia"));
			System.out.println("Vendedor: " + request.getParameter("vendedor"));
			System.out.println("Titulo: " + request.getParameter("title"));
			System.out.println("Descripcion: " + request.getParameter("descrip"));
			
			List<Producto> list = productManager.advSearch(request.getParameter("categoria"), request.getParameter("provincia"), request.getParameter("vendedor"), request.getParameter("title"), request.getParameter("descrip"));
			
			if(list !=null && !list.isEmpty()){
				Imagene img10;
				String img_name10 = "";
				BufferedImage img_buff10;
				List <String> path10 = new ArrayList <String> ();	
				
				
				for (int i = 0; i < list.size(); i++){				
					/*Obtenemos las imagenes*/
					img10 = imagenmanager.obtenerImagenesByProducto(list.get(i).getId()).get(0);				
					
					img_name10 = new Integer(list.get(i).getId()).toString() + new Integer(img10.getId().getNumImg()).toString();
					
					img_buff10 = ImageIO.read(new ByteArrayInputStream(img10.getImagen()));
					
					File img_file = new File (path + img_name10 + ".jpg");
					
					ImageIO.write(img_buff10, "jpg", img_file);
					
					path10.add("temp/" + img_name10 + ".jpg");
				}
		
				request.setAttribute("adv_search_res", list);
				request.setAttribute("adv_search_img_path", path10);
			
			}
			reqDis = request.getRequestDispatcher("results.jsp");
			reqDis.forward(request, response);			
			break;
			
		/*Modificar datos de producto*/	
		case 11:
			// Se comprueba si hayu una sesion activa
			if(request.getSession(false) == null){
				System.out.println("Se ha intentado cerrar una sesion sin sesion o sesion caducada");
				HttpSession session = request.getSession(true);
				session.setAttribute("err", "Parece que no ha iniciado sesi�n, o que esta ha caducado.");
				reqDis = request.getRequestDispatcher("errorHasOccurred.jsp");
				reqDis.forward(request, response);
			}
			
			List <Imagene> imagenesU = null;
			
			/*Obtenemos el propietario del producto*/
			Usuario usuario1 = userManager.findUsuarioByEmail(request.getSession().getAttribute("e_mail").toString());
			
			/*Actualizamos los datos del producto. */
			productManager.modificarProducto(Integer.parseInt(request.getParameter("id_oculto")), request.getParameter("category"), Integer.parseInt(request.getParameter("estado")), 0, Float.parseFloat(request.getParameter("precioP")), request.getParameter("nombreP"), request.getParameter("Comentarios"), imagenesU, usuario1);
			
			
			//Producto productUpdate = productManager.findProductoByID(Integer.parseInt(request.getParameter("id_oculto")));
			//productUpdate.setCategoria(request.getParameter("category"));
			//productUpdate.setEstado(Integer.parseInt(request.getParameter("estado")));
			/*numIMg*/
			//productUpdate.setPrecio(Float.parseFloat(request.getParameter("precioP")));
			//productUpdate.setTitulo(request.getParameter("nombreP"));
			//productUpdate.setDescripcion(request.getParameter("Comentarios"));
			
			Producto productoActualizado = productManager.findProductoByID(Integer.parseInt(request.getParameter("id_oculto")));
			
			
			
			/*Modificamos las imagenes si el usuario ha introducido alguna en el formulario*/
			
			String imagenRequestU = "";
			InputStream inputStreamU;
			Part filePartU;
			ImagenePK idPKU = new ImagenePK ();
			
			
			for (int i = 0; i < 4; i ++){
				
				inputStreamU = null;
				filePartU = null;
				
				switch (i){
				
				case (0):
					
					imagenRequestU = "imagenP1O";
					idPKU.setNumImg(0);
					
					break;
					
				case (1):
					
					imagenRequestU = "imagenP2O";
					idPKU.setNumImg(1);
					break;
					
				case (2):
					
					imagenRequestU = "imagenP3O";
					idPKU.setNumImg(2);
					break;
					
				case (3):
				
					imagenRequestU = "imagenP4O";
					idPKU.setNumImg(3);
					break;
				
				default:			
			
				}
				
				filePartU = request.getPart(imagenRequestU);
				

		            
				if (filePartU != null && filePartU.getSize()> 0 && !filePartU.getContentType().equals("application/octet-stream")) {
					System.out.println("He detectado una insercci�n de imagen");
					
		          /*  System.out.println(filePartU.getName());
		            System.out.println(filePartU.getSize());
		            System.out.println(filePartU.getContentType());*/
					
					inputStreamU = filePartU.getInputStream();
					
					idPKU.setIdProd(productoActualizado.getId());
					
					//int a = imagenmanager.obtenerImagenByIdImagen(idPKU).getId().getIdProd();
					//int b =  imagenmanager.obtenerImagenByIdImagen(idPKU).getId().getNumImg();
					//System.out.println("El id_prod de la imagenActualiazada es: "+ a + " y el num_img es: "+ b);
					
					
					
					byte[] dataU = new byte[(int) filePartU.getSize()];
					int nReadU;
					ByteArrayOutputStream bufferU = new ByteArrayOutputStream();
					while ((nReadU = inputStreamU.read(dataU, 0, dataU.length)) != -1) {
					  bufferU.write(dataU, 0, nReadU);
					}
		
					bufferU.flush();
					
					/*Necesito hacer la actualizacion. Para hacer la actualizacion, necesito poder buscar la imagen que quiero PK*/
					//imagenmanager.insertarImagen(idPKU, bufferU.toByteArray(), productoActualizado);
					
					if (i < productoActualizado.getNumImg()){
						imagenmanager.modificarImagen(idPKU, bufferU.toByteArray(), productoActualizado);
					}
					
					else {
						imagenmanager.insertarImagen(idPKU, bufferU.toByteArray(), productoActualizado);
					}
				}
				
			}
			
			
			request.setAttribute("precioPerfil", productoActualizado.getPrecio());
			request.setAttribute("nombrePerfil", productoActualizado.getTitulo());
			request.setAttribute("categoriaPerfil", productoActualizado.getCategoria());
			request.setAttribute("descripcionPerfil", productoActualizado.getDescripcion());
			request.setAttribute("estadoPerfil", productoActualizado.getEstado());
			
			request.setAttribute("usuarioPerfil", request.getParameter("usuarioPerfil"));
			request.setAttribute("id", request.getParameter("id_oculto"));
			request.setAttribute("ciudad", request.getParameter("provincia_oculta"));
			request.setAttribute("codPostal", request.getParameter("cod_postal_oculta"));
			
			
			
			img_nameR = "";
			InputStream inRU;
			BufferedImage fichero_imagenRU;
			rutasR = new ArrayList <String> ();	
			/*Nos recorremos las imagenes del producto del que venimos y las mostramos*/
			
			/*Obtenemos de nuevo el producto, con el numImg modificado (por trigger)*/
			productoActualizado = productManager.findProductoByID(Integer.parseInt(request.getParameter("id_oculto")));
			
			for (int i = 0; i < productoActualizado.getNumImg(); i++){
				
				imgPR = imagenmanager.obtenerImagenesByProducto(Integer.parseInt(request.getParameter("id_oculto"))).get(i);
				
				//int hola = imgPR.getId().getNumImg();
				//System.out.println("El numImg obtenido es: " + hola);
				
				/*Establecemos el nombre del archivo en funcion de el ID del rpdocuto y el numero de imagen*/
				img_nameR = Integer.toString(imgPR.getId().getIdProd()) + imgPR.getId().getNumImg();
				
				inRU= new ByteArrayInputStream(imgPR.getImagen());
				
				fichero_imagenRU = ImageIO.read(inRU);
				
				File img_saveR = new File (path + img_nameR + ".jpg");
				
				ImageIO.write(fichero_imagenRU, "jpg", img_saveR);
				
				rutasR.add("temp/" + img_nameR + ".jpg");
				System.out.println("Desde product.jsp hemos insertado en esta ruta: " + rutasR.get(i));
				
			}
			
			request.setAttribute("rutasR", rutasR);
			request.setAttribute("numImgR", productoActualizado.getNumImg());
			
			/*En realidad, deberia hacer una consulta a la DB para coger los datos realmente actualizados, pero devolvemos las peticiones para optimizar*/
			//request.setAttribute("precioPerfil", request.getParameter("precioPerfil"));
			//request.setAttribute("nombrePerfil", request.getParameter("nombrePerfil"));
			//request.setAttribute("categoriaPerfil", request.getParameter("categoriaPerfil"));
			//request.setAttribute("usuarioPerfil", request.getParameter("usuarioPerfil"));
			
			//request.setAttribute("descripcionPerfil", request.getParameter("Comentarios"));
			//request.setAttribute("estadoPerfil", request.getParameter("estado"));
			//request.setAttribute("id", request.getParameter("id_oculto"));
			
			//request.setAttribute("ciudad", request.getParameter("provincia_oculta"));
			//request.setAttribute("codPostal", request.getParameter("cod_postal_oculta"));
			
			/*request.getParameter("Comentarios"), imagenesU, usuario1*/
			/*A�adir a la request los nuevos datos*/
			
			reqDis = request.getRequestDispatcher("product.jsp");
			reqDis.forward(request, response);
			
			break;
			


			

			/*Chats*/	
			case 12:

				/*Se comprueba que hay sesion*/
				if(request.getSession(false) == null || request.getSession(false).getAttribute("e_mail") == null){
					
					/*Se inidica por consola el error*/
					System.out.println("Se ha intentado abrir los chats de un usuario sin sesion o sesion caducada");
					
					/*El error 1 siempre indica que se ha eliminado la sesion*/
					request.setAttribute("error", 1);
					
					/*Se devuelve al usuario a la pagina de inicio*/
					reqDis = request.getRequestDispatcher("index.jsp");
					reqDis.forward(request, response);
					
					
				}
				
				/*Si hay sesion*/
				else{

					/*Se crea un string auxiliar donde se recibiran los nuevos mensajes*/
					ArrayList<String> new_mensajes = null; 
					
					if(request.getParameter("accion_chat")!=null){
						
						/*Se crea el objeto para interactuar con la cola*/
						JMSManager mq = new JMSManager();
						
						/*Si al acción es 2 significa una escritura*/
						if(request.getParameter("accion_chat").equals("2")){
							
							/*Se comprueba si el usuario escogido existe*/
							if(userManager.findUsuarioByEmail(request.getParameter("destinatario")) == null){
								/*Se pone el valor de error a 12 para indicar que el usuario escogido no existe*/
									request.setAttribute("error", 12);
							}
							/*Si el usuario existe se escribe el mensaje en la cola*/
							else{
								/*Se llama a la función correspondiente para escribir en la cola de mensajes*/
								mq.escrituraJMS(request.getParameter("new_mensaje"), (String) request.getSession(false).getAttribute("e_mail"), request.getParameter("destinatario"));
							}
						}
						
						/*Si la accion es 1 significa la lectura de mensajes*/
						else if(request.getParameter("accion_chat").equals("1")){
							
							/*Se obtienen los mensajes de la cola de mensajes correspondientes al usuario*/
							new_mensajes = mq.lecturaJMS((String) request.getSession(false).getAttribute("e_mail"));
			
							/*Se introducen los mensajes en la request*/
							request.setAttribute("mensajes", new_mensajes);
							
							/*Se pone el error a 13 dbeido a que indica que no se disponen de mensajes nuevos*/
							if(new_mensajes == null || new_mensajes.isEmpty())
								request.setAttribute("error", 13);
						}
					}
					
					request.setAttribute("observando_chats",1);
					
					/*Se envia al administrador a la pagina de administracion*/
					reqDis = request.getRequestDispatcher("perfil.jsp");
					reqDis.forward(request, response);
					
				}
				break;
			
			/*Busqueda simple*/
			case 13:
				
				/*Se comprueba que hay sesion*/
				if(request.getSession(false) == null || request.getSession(false).getAttribute("e_mail") == null){
					
					/*Se inidica por consola el error*/
					System.out.println("Se ha intentado administrar a los usuarios sin sesion o sesion caducada");
					
					/*El error 1 siempre indica que se ha eliminado la sesion*/
					request.setAttribute("error", 1);
					
					/*Se devuelve al usuario a la pagina de inicio*/
					reqDis = request.getRequestDispatcher("index.jsp");
					reqDis.forward(request, response);
					
				}
				/*Si hay sesion*/
				
				else{
				/*Lista de productos obtenidos en la busqueda simple*/
				List<Producto> simple_search = null;
				
				/*Si la categoria es distinta de la de por defecto*/
				if(!request.getParameter("category").equals("Todo")){
					
					/*Si la busqueda no es vacia, se busca en dicha categoria las coincidencias de la busqueda en titulo y descripción*/
					if(!request.getParameter("search").isEmpty()){
						simple_search = productManager.obtenerAllProducts_wordsAndCategory(request.getParameter("search"),request.getParameter("category"));
					}
					/*Si la busqueda es vacia, se dan todos los productos de la correspondiente categoria*/
					else{
						simple_search = productManager.findProductoByCategoria(request.getParameter("category"));
					}

				}
				
				/*Si la categoria es por defecto*/
				else{
					/*Si la busqueda no es vacia, se busca en todas las categorias las coincidencias de la busqueda en titulo y descripcion*/
					if(!request.getParameter("search").isEmpty()){
						simple_search = productManager.obtenerAllProducts_words(request.getParameter("search"));
					}
					/*Si tanto la busqueda como la categoria son vacias se devuelven todos los productos*/
					else{
						simple_search = productManager.obtenerAllProducts();
					}
					
				}
				if(simple_search != null && !simple_search.isEmpty()){
					Imagene img10;
					String img_name10 = "";
					BufferedImage img_buff10;
					List <String> path10 = new ArrayList <String> ();	
					
					
					for (int i = 0; i < simple_search.size(); i++){				
						/*Obtenemos las imagenes*/
						img10 = imagenmanager.obtenerImagenesByProducto(simple_search.get(i).getId()).get(0);				
						
						img_name10 = new Integer(simple_search.get(i).getId()).toString() + new Integer(img10.getId().getNumImg()).toString();
						
						img_buff10 = ImageIO.read(new ByteArrayInputStream(img10.getImagen()));
						
						File img_file = new File (path + img_name10 + ".jpg");
						
						ImageIO.write(img_buff10, "jpg", img_file);
						
						path10.add("temp/" + img_name10 + ".jpg");
					}
					
					request.setAttribute("adv_search_res", simple_search);
					request.setAttribute("adv_search_img_path", path10);
				}
				reqDis = request.getRequestDispatcher("results.jsp");
				reqDis.forward(request, response);	

			}
				break;
				
			case 14:
				// Se comprueba si hayu una sesion activa
				if(request.getSession(false) == null){
					System.out.println("Se ha intentado cerrar una sesion sin sesion o sesion caducada");
					HttpSession session = request.getSession(true);
					session.setAttribute("err", "Parece que no ha iniciado sesi�n, o que esta ha caducado.");
					reqDis = request.getRequestDispatcher("errorHasOccurred.jsp");
					reqDis.forward(request, response);
				}
				
						
				productManager.BajaProducto(productManager.findProductoByID(Integer.parseInt(request.getParameter("id_oculto"))));
				
				reqDis = request.getRequestDispatcher("index.jsp");
				reqDis.forward(request, response);
					

		default:
			System.out.println("no se puede llegar a este punto");
			HttpSession session = request.getSession(true);
			session.setAttribute("err", "Parece que ha habido un error.");
			reqDis = request.getRequestDispatcher("errorHasOccurred.jsp");
		}

	}

}
